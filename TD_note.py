#!/usr/bin/env python
# coding: utf-8

# # Import modules

# In[2]:


import numpy as np
import torch
import torchvision
import matplotlib.pyplot as plt
from time import time
from torchvision import datasets, transforms
from torch import nn, optim
import os 
from PIL import ImageFile, Image
ImageFile.LOAD_TRUNCATED_IMAGES = True
from datasets import load_dataset 
import pandas as pd

# resize now doesn't fail


# In[3]:


transform = transforms.Compose([transforms.ToTensor(),
                              transforms.Normalize((0.5,), (0.5,)),
                              ])


# In[4]:


path2 = r"C:\Users\rapha\Desktop\COURSM1BIOINFO\S9\DEA\TDBEURTON\dataset-master\dataset-master\JPEGImages/"
pathlabel2 = r"C:\Users\rapha\Desktop\COURSM1BIOINFO\S9\DEA\TDBEURTON\dataset-master\dataset-master\labels.csv"


# In[5]:


def parcours_train(sous_rep):
    dataset = []
    filename = []
    path = sous_rep
    label = sous_rep 
    h=256
    w=256
    for imgPath in os.listdir(path ):
        img = Image.open(path + imgPath).resize((h, w))
        #img.resize((h, w))
        #print(str(imgPath))
        img.load()
        img=np.asarray(img,dtype=np.uint8)
        #img=torch.from_numpy(img)
        img = transform(img) 
        dataset.append(img)
        name = str(imgPath)
        filename.append(name) 
    return dataset,filename 


dataset,names = parcours_train(path2)


# # Encodage des labels 

# In[6]:


label = pd.read_csv(pathlabel2)


# In[7]:


label = label.dropna(subset=['Category'])


# In[8]:


label.Category.value_counts() 


# In[9]:


def filter_classes(label, data, desired_classes):
    # Vérifier que le nombre de classes dans label et data est le même
    assert len(label) == len(data), "Le nombre d'étiquettes et de données doit être le même."

    # Créer un masque pour filtrer les classes désirées
    mask = torch.tensor([l in desired_classes for l in label])

    # Filtrer les données et les étiquettes
    filtered_label = label[mask]
    filtered_data = data[mask]

    return filtered_label, filtered_data


# In[10]:


label.Category = pd.Categorical(pd.factorize(label.Category)[0])


# In[11]:


label.drop(['Unnamed: 0'],axis=1)


# In[12]:


class_count = label.Category.value_counts()


# In[14]:


label = label.drop(index=label.index[[280]])
label = label.drop(index=label.index[[116]])


# In[15]:


res = []
for number in label.Category:
    if number != 1000:  
        res.append(number) 
print(res)


# In[16]:


len(res)
print(len(dataset))


# In[17]:


labels = torch.tensor(res) 


# In[18]:


trainloader = torch.utils.data.DataLoader(dataset, batch_size=365, shuffle=True)
valloader = torch.utils.data.DataLoader(label, batch_size=365, shuffle=True)
#labels avec s 
dataiter = iter(trainloader)
images = next(dataiter)


print(label.shape, images.shape)


# In[19]:




labels, images = filter_classes(labels,images,[0,2,5,4])
print(labels.shape, images.shape)


# In[20]:


def replace_labels(tensor):
    # Remplacer les valeurs dans le tensor
    tensor = torch.where(tensor == 0, torch.tensor(0), tensor)
    tensor = torch.where(tensor == 2, torch.tensor(1), tensor)
    tensor = torch.where(tensor == 5, torch.tensor(2), tensor)
    tensor = torch.where(tensor == 4, torch.tensor(3), tensor)
    
    return tensor


labels_after_conversion = replace_labels(labels)

unique_values, counts = torch.unique(labels_after_conversion, return_counts=True)
counts_dict = dict(zip(unique_values.numpy(), counts.numpy()))


print("Décompte des valeurs après conversion :", counts_dict)
print("0 = NEUTROPHIL, 1 = EOSINOPHIL, 2 = LYMPHOCYTE, 3 = MONOCYTE " )
labels = labels_after_conversion


# In[21]:


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
images, labels = images.to(device), labels.to(device)


# In[22]:


images


# # Transformation des données 

# ## OVERSAMPLING 

# # Construction d'un CNN 

# ## SPlit des datas 

# In[23]:


from torch.utils.data.dataloader import DataLoader
from torch.utils.data import random_split
from sklearn.model_selection import train_test_split

import torch 


train_data,val_data,train_label,val_label = train_test_split(images,labels,test_size=0.1,random_state=42)


# ## Définition de différentes variables pour le ML 

# In[24]:


from torch.utils.data import TensorDataset, DataLoader, WeightedRandomSampler
from collections import Counter


def fonction_over(dataset,label):
    #transformation du label en list  
    labels_list = label.tolist() 

    #calcul des fréquences
    class_count = Counter(labels_list)

    
    num_sample= len(labels_list)
    class_weights = {cls: num_sample / count for cls, count in class_count.items()}

    weights = [class_weights[label] for label in labels_list]
    print(len(weights), weights)
    weights = torch.DoubleTensor(weights)
    sampler = WeightedRandomSampler(weights, len(weights))

    return sampler 




# In[51]:


import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torch.utils.data import WeightedRandomSampler
import torch.nn as nn
import torch.nn.functional as F

# Définir le modèle CNN
class SimpleCNN(nn.Module):
    def __init__(self, num_classes=4):
        super(SimpleCNN, self).__init__()
        self.conv1 = nn.Conv2d(3, 16, kernel_size=3, padding=1)
        self.batch_norm1 = nn.BatchNorm2d(16)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(16, 8, kernel_size=3, padding=1)
        self.batch_norm2 = nn.BatchNorm2d(8)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv3 = nn.Conv2d(8, 4, kernel_size=3, padding=1)
        self.batch_norm3 = nn.BatchNorm2d(4)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.fc1 = nn.Linear(4 * 32 * 32, 32)
        self.fc2 = nn.Linear(32, 16)
        self.fc3 = nn.Linear(16, 8)
        self.fc4 = nn.Linear(8, num_classes)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.pool1(self.batch_norm1(nn.functional.relu(self.conv1(x))))
        x = self.pool2(self.batch_norm2(nn.functional.relu(self.conv2(x))))
        x = self.pool3(self.batch_norm3(nn.functional.relu(self.conv3(x))))
        x = x.view(-1, 4 * 32 * 32)
        x = nn.functional.relu(self.fc1(x))
        x = nn.functional.relu(self.fc2(x))
        x = nn.functional.relu(self.fc3(x))
        x = self.fc4(x)
        #x = nn.LogSoftmax(dim=1)(self.fc4(x))
        #x = self.softmax(x)

        return x



# Définir le jeu de données personnalisé
class CustomDataset(Dataset):
    def __init__(self, images, labels):
        self.images = images
        self.labels = labels

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        image = self.images[idx]
        label = self.labels[idx]

        # Assurez-vous que l'image est en format torch.Tensor
        image = torch.tensor(image, dtype=torch.float32)
        
        return image, label

# Transformer les données en tenseurs PyTorch
images_tensor = train_data  # Remplacez cela par vos propres données
labels_tensor = train_label  # Remplacez cela par vos propres données
#images_tensor = torch.tensor(images_tensor, dtype=torch.float32).to(device)



# Créer une instance du modèle
#model =  SimpleCNN() 
model = torch.hub.load('pytorch/vision:v0.10.0', 'resnet18', pretrained=False)
model.to(device)
# Créer une instance du jeu de données
dataset = CustomDataset(images_tensor, labels_tensor)

# Définir les paramètres d'entraînement
batch_size = 128
num_epochs = 20
learning_rate =  0.01

#Oversampling
'''
unique_values, counts = torch.unique(labels_tensor, return_counts=True)
counts_dict = dict(zip(unique_values.numpy(), counts.numpy()))

sample_weight = [1/counts_dict[0],1/counts_dict[1],1/counts_dict[2],1/counts_dict[3]]
sampler = WeightedRandomSampler(sample_weight, len(images_tensor),replacement=True)


class_weights = compute_class_weights(images_tensor, labels_tensor)
weights = [class_weights[labels_tensor] for data, label in images_tensor]
sampler = WeightedRandomSampler(weights, len(weights))
'''

sampler = fonction_over(images_tensor,labels_tensor) 

# Créer un DataLoader pour le jeu de données
dataloader = DataLoader(dataset, batch_size=batch_size,sampler=sampler)

# Définir la fonction de perte et l'optimiseur
criterion = nn.CrossEntropyLoss()
optimizer = optim.AdamW(model.parameters(), lr=learning_rate, weight_decay=1e-5)


# Ajouter un Learning Rate Scheduler
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.1)

# Entraîner le modèle
for epoch in range(num_epochs):
    # Dans la boucle d'entraînement
    #model.train()
    #scheduler.step()
    for inputs, labels in dataloader:
        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

    print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item()}')

# Enregistrez le modèle si nécessaire
torch.save(model.state_dict(), 'simple_cnn_model.pth')


# In[48]:


dataset_test= CustomDataset(val_data, val_label)
#sampler = fonction_over(val_data,val_label)
test_dataloader = DataLoader(dataset_test, batch_size=batch_size)

model.eval()  # Mettre le modèle en mode évaluation
with torch.no_grad():
    for inputs, labels in test_dataloader:
        outputs = model(inputs)
        predictions = torch.argmax(outputs, dim=1)
        # Calculer les métriques d'évaluation ici

print(predictions)
print(val_label)


# In[49]:


from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt

# Obtenez les prédictions et les étiquettes réelles
all_predictions = []
all_labels = []

model.eval()  
with torch.no_grad():
    for inputs, labels in test_dataloader:
        outputs = model(inputs)
        predictions = torch.argmax(outputs, dim=1)
        all_predictions.extend(predictions.numpy())
        all_labels.extend(labels.numpy())


classes = ["NEUTROPHIL", "EOSINOPHIL", "LYMPHOCYTE", "MONOCYTE"]
#print("0 = NEUTROPHIL, 1 = EOSINOPHIL, 2 = LYMPHOCYTE, 3 = MONOCYTE " )

# Créer une matrice de confusion
cm = confusion_matrix(all_labels, all_predictions)
print(cm)

# Afficher la matrice de confusion avec seaborn
plt.figure(figsize=(10, 8))
sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", xticklabels=classes, yticklabels=classes)
plt.xlabel("Prédictions")
plt.ylabel("Vraies étiquettes")
plt.show()


# In[44]:



from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, classification_report

# Calcul de l'accuracy
accuracy = accuracy_score(all_labels, all_predictions)
print(f'Accuracy: {accuracy}')

# Calcul de la precision, du rappel et de la F1-score
precision = precision_score(all_labels, all_predictions, average='weighted')
recall = recall_score(all_labels, all_predictions, average='weighted')
f1 = f1_score(all_labels, all_predictions, average='weighted')

print(f'Precision: {precision}')
print(f'Recall: {recall}')
print(f'F1-score: {f1}')

# Affichage du rapport de classification (contenant precision, recall, f1-score par classe)
class_report = classification_report(all_labels, all_predictions, target_names=classes)
print('Classification Report:\n', class_report)


# In[29]:


val_data.shape


# # Construction CNN V2 
